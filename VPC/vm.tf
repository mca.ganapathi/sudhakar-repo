resource "aws_instance" "demo" {
  ami           = "ami-0b59bfac6be064b78"
  instance_type = "t2.micro"

   vpc_security_group_ids = ["${aws_security_group.ssh_allowed.id}" ]
   subnet_id = aws_subnet.my_subnet_public.id
   key_name = "demo-test"
   associate_public_ip_address = true
  
  tags = {
    name = "demo-test"
   } 
}
