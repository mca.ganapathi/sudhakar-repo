resource "aws_sns_topic" "user_updates" {
  name = "user-updates-topic"
  kms_master_key_id = "alias/aws/sns"
}
resource "aws_sns_topic_subscription" "email-target" {
  topic_arn = aws_sns_topic.user_updates.arn
  protocol  = "email"
  endpoint  = var.sns_subscription_email_address_list
}
