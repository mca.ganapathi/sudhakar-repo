resource "aws_cloudwatch_log_metric_filter" "demo" {
  name           = "MyAppAccessCount"
  pattern        = "ERROR"
  log_group_name = aws_cloudwatch_log_group.log group.name

  metric_transformation {
    name      = "ErrorCount"
    namespace = "YourNamespace"
    value     = "1"
  }
}
