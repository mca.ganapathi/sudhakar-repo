resource "aws_cloudtrail" "CloudTrail" {
  name = "ManagementEventsTrail"
  s3_bucket_name = aws_s3_bucket.S3BucketForCloudTrail.id
  is_multi_region_trail = true
  enable_log_file_validation = true
  cloud_watch_logs_group_arn = "arn:aws:logs:us-east-2:712232843848:log-group:loggroup1:*"
  cloud_watch_logs_role_arn = aws_iam_role.CwLogIamRole.arn

  event_selector {
    include_management_events = true
    read_write_type = "All"
  }
}

resource "aws_s3_bucket" "S3BucketForCloudTrail" {
  bucket = "demo-test0983674"
  policy = <<POLICY
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "AWSCloudTrailAclCheck",
            "Effect": "Allow",
            "Principal": {
              "Service": "cloudtrail.amazonaws.com"
            },
            "Action": "s3:GetBucketAcl",
            "Resource": "arn:aws:s3:::demo-test0983674"
        },
        {
            "Sid": "AWSCloudTrailWrite",
            "Effect": "Allow",
            "Principal": {
              "Service": "cloudtrail.amazonaws.com"
            },
            "Action": "s3:PutObject",
            "Resource": "arn:aws:s3:::demo-test0983674/AWSLogs/*",
            "Condition": {
                "StringEquals": {
                    "s3:x-amz-acl": "bucket-owner-full-control"
                }
            }
        }
    ]
}
POLICY
}

resource "aws_iam_role" "CwLogIamRole" {
  name = "IamCloudTrail"
  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": ["cloudtrail.amazonaws.com"]
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
POLICY
}

resource "aws_iam_role_policy" "CwLogIamRoleInlinePolicyRoleAttachment0" {
  name = "allow-access-to-cw-logs"
  role = aws_iam_role.CwLogIamRole.id
  policy = <<POLICY
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "logs:CreateLogStream",
                "logs:PutLogEvents"
            ],
            "Resource": "*"
        }
    ]
}
POLICY
}


