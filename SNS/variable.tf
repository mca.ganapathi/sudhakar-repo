variable "sns_subscription_email_address_list" {
  type = string
  description = "List of email addresses as string(space separated)"
}
