resource "aws_iam_user" "demo" {
  name = "demo-test"
  path = "/system/"

  tags = {
    Name = "demo-test"
  }
}

resource "aws_iam_access_key" "test" {
  user = aws_iam_user.demo.name
}
