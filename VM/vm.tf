resource "aws_instance" "demo" {
  ami           = "ami-0b59bfac6be064b78"
  instance_type = "t2.micro"
   key_name = "demo-test"
   associate_public_ip_address = true
  
  tags {
    name = "demo-test"
   } 
}
