resource "aws_route_table" "my_route_public"{
   vpc_id = aws_vpc.my_vpc.id 
   route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.my_internet_gateway.id
  }
   tags = {
      Name = my_route_public   
   }
}

resource "aws_route_table_association" "public"{
   subnet_id = aws_subnet.my_subnet_public.id
   route_table_id = aws_route_table.my_route_public.id
}
